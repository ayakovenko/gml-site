Active: yes
Category: courses
Id: introduction_to_medical_image_analysis
Title: Introduction to medical image analysis
Image: images/missing_image.jpg
Teachers: olga_senyukova
CourseOrder: 15

This course is devoted to application of modern deep learning algorithms to medical image analysis tasks. Introductory lecture contains information about different kinds of medical imaging technologies, as well as overview of current challenges and practical applications of medical image analysis algorithms in Russia and in the world. The main part of the course includes theoretical and practical material on convolutional neural networks, especially U-Net and its modifications, GANs, unsupervised and semi-supervised learning and also general issues of working on medical image analysis projects. The course is accompanied by the practical task where the students are free in choosing the algorithms.
