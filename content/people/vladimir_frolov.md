Active: yes
Category: people
PersonType: staff
Id: vladimir_frolov
Name: Vladimir
Surname: Frolov
PublicationsPossibleNames: Владимир
PublicationsPossibleSurnames: Фролов
ParseNewPublications: yes
LabGroups: computer_graphics_group
PersonOrder: 10
Position: Researcher
Email: vladimir.frolov@graphics.cs.msu.ru
IstinaPage: https://istina.msu.ru/profile/VladimirFrolov/
Photo: images/people/vladimir_frolov.jpg
ResearchInterests: computer graphics
Projects:


