Active: yes
Category: people
PersonType: staff
Id: dmitriy_vatolin
Name: Dmitriy
Surname: Vatolin
PublicationsPossibleNames: Дмитрий
PublicationsPossibleSurnames: Ватолин
ParseNewPublications: yes
LabGroups: video_group
PersonOrder: 0
Position: Senior Researcher, Head of Lab
Email: dmitriy@graphics.cs.msu.ru
IstinaPage: https://istina.msu.ru/profile/dmitriyv/
GoogleScholarPage: https://scholar.google.com/citations?user=545J9E4AAAAJ
Photo: images/people/dmitriy_vatolin.jpg
ResearchInterests:
Projects: video_matting_benchmark

Dmitriy Vatolin graduated from the Applied Mathematics department of Moscow
State University in 1996. He defended his Ph.D. thesis on computer graphics in
2000. A co-author of a book on data compression (Russian), published in 2003. A
co-founder of compression.ru website — one of the biggest site on data
compression and video processing in the world. From 2000 until 2006 took part
in 5 start-up computer companies, in 3 as a co-founder, 4 of the companies are
operating until now.

# Research interests

## Video and data compression
* MP3 ZIP - lossless compressor of MP3 files with 1.2 ratio
* Visicron Cyclon codec - was the best for videoconferences streams during 2001-2004.
* MSU Lossless Video Codec - was the best in 2005-2007
* MSU annual H.264 codec comparisons

## Video processing

* Set of "MSU video filters" - research prototypes, tested with wide auditory
  (more than 3000000 downloads, strong feedback)
* Deinterlacer, Frame Rate Conversion and other filters for Samsung TV-sets and
  Broadcom STB.
* MSU Video Quality Metrics Tool - the biggest amount of video quality metrics,
  most was created during projects of self-tuning video filters

## 3D video
* Depth from Motion, Focus, Effects and Geometry - set of semi-automatic tools
  for fast depth reconstruction
* Depth Propagation and depth editing - set of tools for accurate depth editing
* Robust practical and fast GPU-accelerated Optical Flow
* Stereo-multiview Generator with Matting and special occlusion processing
* Stereo movies testing metrics (Focus mismatch, Color mismatch, Vertical
  parallax, Channels mismatch and etc).
* 3D devices testing base
