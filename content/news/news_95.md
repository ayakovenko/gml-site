Active: yes
Title: In memory of Yuri Bayakovsky
Category: news
Date: 2014-06-17 00:00

![In memory of Yuri Bayakovsky]({static}/images/news/in_memory_of_yuri_bayakovsky.jpg "In memory of Yuri Bayakovsky")

Prof. Yuri M. Bayakovsky, head of our laboratory, has passed on June 17, 2014 after long illness. He had established our laboratory and taught several generations of students. All his life was devoted to computer graphics in Russia. He made an invaluable contribution to science. We will always remember him as a good person and mentor.
