Active: yes
Title: Лаборатория примет участие в конференции CVPR 2010
Category: news
Date: 2010-03-19 00:00

Статья сотрудника Лаборатории [Бариновой О.С.]({filename}/people/olga_barinova.md) была принята на ведущую мировую конференцию Computer Vision and Pattern Recognition ([CVPR 2010](http://cvl.umiacs.umd.edu/conferences/cvpr2010/)). Научная работа была выполнена в рамках совместного проекта Лаборатории и Microsoft Research. Статья посвящена поиску объектов на изображениях.
Сотрудники лаборатории искренне поздравляют [Баринову О.С.]({filename}/people/olga_barinova.md) с научным успехом!
