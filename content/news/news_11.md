Active: yes
Title: Лаборатория на выставке 25 ноября
Category: news
Date: 2009-12-01 00:00

25 ноября [прошло](http://news.kremlin.ru/news/6106) заседание Комиссии по модернизации и технологическому развитию экономики России. Лаборатория приняла участие в выставке инновационных проектов, приуроченной к данному мероприятию.
