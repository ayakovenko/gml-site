Active: yes
Title: Congrats to Elena Tretyak
Category: news
Date: 2011-06-21 00:00

Master Thesis of our student Elena Tretyak "Geometric image parsing in man-made environments" won the first prize at the competition of Master Thesises at CMC MSU. The work was supported by Microsoft Research and supervised by [Olga Barinova]({filename}/people/olga_barinova.md) and [Anton Konushin]({filename}/people/anton_konushin.md). Congrats to Lena!
