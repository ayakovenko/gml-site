Active: yes
Title: Defense of Ph.D. thesis of Konstantine Strelnikov
Category: news
Date: 2009-05-13 00:00

Defense of Ph.D. thesis of Konstantine Strelnikov will take place on June 9th, 2009, at 11 a.m. in conference room of Keldsyh Institute of Applied Mathemathics (you can [download thesis in russian](http://keldysh.ru/council/1/strelnikov.pdf))
