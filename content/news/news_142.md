Active: yes
Title: CMC students won Vision Hack, International students AI Hackathon
Category: news
Date: 2017-10-03 00:00

CMC MSU graduate students team DoubleA Team of Andrey Belyaev, Alexander Gromov and Konstantin Sophiuk won 1st Prize at Visual Hack, one of the largest Artificial Intelligence and Computer Vision Hakathons for drones.

Another CMC MSU students team GMLvision of Ilya Petrov, Sergey Dukanov and Vladimir Guzov won 3rd Prize.

27 commands from Russia, USA, Great Britain, PRC and Spain took part in competition at National University of Science and Technology (MISIS), during Sept 10 - Sept 13.

[Details >>](http://www.msu.ru/news/ctudenty-vmk-pobediteli-khakatona-po-kompyuternomu-zreniyu.html)
