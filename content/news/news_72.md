Active: yes
Title: Timur Ibadov, Sergey Ushakov and Alexander Velizhev won the competition for Point Cloud Library
Category: news
Date: 2013-04-27 00:00

Timur Ibadov, Sergey Ushakov and Alexander Velizhev won the competition for participation in development of Point Cloud Library ([http://pointclouds.org](http://pointclouds.org)) within Code Sprint 2013, sponsored by  Honda Research Institute. The Point Cloud Library (PCL) is a standalone, large scale, open source project for 3D point cloud processing. The team will implement and integrate into the library two modern algorithms for 3D point cloud recognition.
