Active: yes
Title: Congratulations to Olga Senyukova and Mikhail Erofeev who have received MSU scholarship
Category: news
Date: 2014-12-24 00:00

Congratulations to [Olga Senyukova]({filename}/people/olga_senyukova.md) and [Mikhail Erofeev]({filename}/people/mikhail_erofeev.md) who have received [MSU scholarship for young teachers and researchers](http://www.msu.ru/science/prem/stipendii-i-granty-2015-goda.php)
