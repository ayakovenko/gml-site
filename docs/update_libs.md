# Как обновить версии библиотек

Для этого нужно внести изменения в файл `requirements.txt`.
Чтобы обновить используемую версию python, нужно обновить ее в `.gitlab-ci.yml`.
Далее нужно проверить, что сгенерированные локально директории с сайтами не отличаются.
По умолчанию сайт генерируется в директории `public`.
Поэтому можно выполнить последовательность команд:
```
mv public public_old
make publish
diff -qr public public_old
```

## Обновление версии mathjax:
Нужно в `Makefile` в строке:
```
git clone https://github.com/mathjax/MathJax.git mathjax -b 2.7.3;
```
Прописать новую версию вместо `2.7.3`.

## Обновление jquery
Нужно скачать [Отсюда](https://jquery.com/download/) новую `compressed, production` версию.
Дальше содержание файла надо скопировать в `theme/static/js/jquery.js`.
